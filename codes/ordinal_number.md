# Ordinal Numbers Series Program in Java

An ordinal number shows a relative position, e.g., 1st, 2nd,3rd, 4th etc.

Write a program/function to take any number as input and return the ordinal number of that number

Output for the respective numbers:

```
1 = 1st
2 = 2nd
3 = 3rd
4 = 4th
11 = 11th
112 = 112th
110001 = 110001st
1124 = 1124th
500 = 500th
62 = 62nd
77 = 77th
11281 = 11281st
99 = 99th
100000 = 100000th
```
