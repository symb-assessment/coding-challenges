### Print alphabet 'M' using stars

Write a program to print alphabet 'M' pattern using astricks

Take input from user for the size of pattern.

Output will looks like this:

![Alphabet M](/patterns/assets/alphabet_m.png "Alphabet M")
