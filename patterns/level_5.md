### Full star diamond pattern.

Write a program to print full star diamond pattern.

Take input from user for the size of pattern.

Output will looks like this:

![Pattern for demo](/patterns/assets/pattern_full_all.png "Pattern Demo")
