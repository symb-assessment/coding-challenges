### Full star diamond pattern.

Write a program to print full star diamond pattern.

Take input from user for the size of pattern.

Output will looks like this:

![Pattern half diamond all](/patterns/assets/pattern_half_all.png "Pattern half diamond all")
